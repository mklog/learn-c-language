#include<stdio.h>
typedef struct student   /* typedef用于自定义类型，相当于定义int float 等... */
{
   char num[20];
   char name[20];
   int age;
   float score;
}st;                     /*  用st作为关键字来代替 struct student 来定义结构体 */

main()
{
   st n1={"12160600209","Liuminakai",21,99.9};
   st *p;
   clrscr();
   p=&n1;                /* &用于取n1的地址，p指向n1的地址 */
   printf("     ID          Name       Age     Score  \n");
   printf(" %s  %s     %d     %.2f",(*p).num,(*p).name,(*p).age,(*p).score);
}