[Root]
RootPath=..
NoWatch=true
NoPacked=false
NoPhysical=true

[Bin]
scripts

[Physical]
ini=data/ini
statistics=data/statistics
appdata=$(localappdata)/Risen2/Config
snapshots=$(localappdata)/Risen2/ScreenShots
save=$(savedgames)/Risen2/SaveGames
data/extern/videos

[Packed]
;data/raw/animations
;data/raw/cutscenes
;data/raw/dialogue_german=../../data/raw/Speech_German
;data/raw/effects
;data/raw/fonts
;data/raw/images
;data/raw/library
;data/raw/meshes
;data/raw/skinnedmeshes
;data/raw/sounds
;data/raw/speedtrees
;data/raw/strings
data/common/brushes
data/common/gui2
data/common/library
data/common/materials
data/common/meshes
da